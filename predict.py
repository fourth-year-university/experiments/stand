import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def fix_array(arr):
    for i in range(1, len(arr) - 1):
        if arr[i] == 0:
            arr[i] = (arr[i - 1] + arr[i + 1]) // 2
    return arr


def do_job(WIND, SUN, HOUSES, HOSPITALS, FACTORIES):
    my_export = 0
    my_import = 0

    info = {}

    names = [
        ["Солнце", "sun.clf", "sun"],
        ["Ветер", "wind.clf", "wind"],
        ["Заводы", "factories.clf", "factories"],
        ["Больницы", "hospitals.clf", "hospitals"],
        ["Дома", "houses.clf", "houses"]
    ]

    forecast_file = "input/forecast.csv"

    df = pd.read_csv(forecast_file, delimiter='\t')

    for name in names:
        data = open("models/" + name[1], "rb").read()
        clf = pickle.loads(data)

        forecast = []

        for s in df[name[0]]:
            forecast.append([int(s * 1000)])
        info[name[2]] = fix_array(clf.predict(forecast))
        # info[name[2]] = clf.predict([[[0]] * 169])[0]

        # plt.plot(forecast)
        # plt.plot(info[name[2]])
        # plt.show()

    plt.plot(WIND * np.array(info['wind']), label='Ветер')
    plt.plot(SUN * np.array(info['sun']), label='Солнце')
    plt.plot(HOUSES * np.array(info['houses']), label='Дома')
    plt.plot(HOSPITALS * np.array(info['hospitals']), label='Больницы')
    plt.plot(FACTORIES * np.array(info['factories']), label='Заводы')
    plt.legend()
    plt.title("По категориям")
    plt.grid()
    # plt.show()
    plt.savefig("output/categories.png")
    plt.clf()

    energy_balance = WIND * np.array(WIND * np.array(info['wind'])) \
             + SUN * np.array(info['sun']) \
             - HOUSES * np.array(info['houses']) \
             - HOSPITALS * np.array(info['hospitals']) \
             - FACTORIES * np.array(info['factories'])

    plt.plot(WIND * np.array(WIND * np.array(info['wind']))
             + SUN * np.array(info['sun'])
             - HOUSES * np.array(info['houses'])
             - HOSPITALS * np.array(info['hospitals'])
             - FACTORIES * np.array(info['factories']))
    plt.title("Баланс потребления и генерации")
    plt.grid()
    # plt.show()
    plt.savefig("output/balans.png")
    plt.clf()

    plt.plot(WIND * np.array(WIND * np.array(info['wind']))
             + SUN * np.array(info['sun']), label="Генерация")
    plt.plot(
        + HOUSES * np.array(info['houses'])
        + HOSPITALS * np.array(info['hospitals'])
        + FACTORIES * np.array(info['factories']), label="Потребление")

    plt.title("Потребление и генерация")
    plt.legend()
    plt.grid()
    # plt.show()
    plt.savefig("output/cons_gen.png")
    plt.clf()

    plt.plot(1 * np.array(info['wind']), label='Ветер')
    plt.plot(1 * np.array(info['sun']), label='Солнце')
    plt.plot(1 * np.array(info['houses']), label='Дома')
    plt.plot(1 * np.array(info['hospitals']), label='Больницы')
    plt.plot(1 * np.array(info['factories']), label='Заводы')

    plt.title("Прогноз")
    plt.legend()
    plt.grid()
    # plt.show()
    plt.savefig("output/forecast.png")
    plt.clf()

    my_export = 0
    my_import = 0

    for i in range(len(info['wind'])):
        s += WIND * info['wind'][i]
        s += SUN * info['sun'][i]
        s -= HOUSES * info['houses'][i]
        s -= HOSPITALS * info['hospitals'][i]
        s -= FACTORIES * info['factories'][i]

        if s > 0:
            my_export += s
        else:
            my_import -= s

    with open('output/import_export.txt', 'w') as f:
        f.write(f"{int(my_import)} {int(my_export)}")

    modes1 = ['WIND', 'SUN']
    modes2 = ['HOUSE', 'HOSPITAL', 'FACTORY']
    story = [0]
    loses = 0
    profits = 0
    for mode in modes1:
        f = open("input/" + mode)
        data = list(map(float, f.read().split()))
        loses += sum(data)
    for mode in modes2:
        f = open("input/" + mode)
        data = list(map(float, f.read().split()))
        profits += sum(data)

    f1 = open('output/buysell.txt', 'w')

    for i in range(170):
        story.append(story[-1] + profits - loses)
        if energy_balance[i - 1] > 0:
            story[i] += energy_balance[i - 1]//1000 * 2
        else:
            story[i] -= energy_balance[i - 1]//1000 * 2
        f1.write(str(energy_balance[i - 1]//1000) + "\n")
    f1.close()


    plt.plot(story, label='Баланс предприятия')

    plt.title("Деньги")
    plt.legend()
    plt.grid()
    # plt.show()
    plt.savefig("output/money.png")
    plt.clf()


if __name__ == "__main__":
    SUN = 1
    WIND = 1
    HOUSES = 3
    HOSPITALS = 1
    FACTORIES = 1
    do_job(SUN, WIND, HOUSES, HOSPITALS, FACTORIES)
