import telebot
from telebot import types
import predict

objects = ""



def update_objects():
    global objects
    global itembtn_a
    global markup_main
    data = open("input/objects.txt").read().split()
    predict.do_job(int(data[0]), int(data[1]), int(data[2]), int(data[3]), int(data[4]))

    import_export = open("output/import_export.txt").read().split()

    objects = f"💨{data[0]} ☀️{data[1]} 🏠{data[2]} 🏥{data[3]} 🏭{data[4]}"

    markup_main = types.ReplyKeyboardMarkup()
    itembtn_a1 = types.KeyboardButton(f"💨{data[0]}")
    itembtn_a2 = types.KeyboardButton(f"☀️{data[1]}")
    itembtn_a3 = types.KeyboardButton(f"🏠{data[2]}")
    itembtn_a4 = types.KeyboardButton(f"🏥{data[3]}")
    itembtn_a5 = types.KeyboardButton(f"🏭{data[4]}")

    itembtn_b1 = types.KeyboardButton(f"📊Прогноз")
    itembtn_b2 = types.KeyboardButton(f"📊Баланс")
    itembtn_b3 = types.KeyboardButton(f"📊Детализированный")
    itembtn_b4 = types.KeyboardButton(f"📊Потребление/генерация")
    itembtn_b5 = types.KeyboardButton(f"📊Деньги")




    itembtn_c3 = types.KeyboardButton(f"Докупка/продажа")



    itembtn_с1 = types.KeyboardButton(f"🟥 import {import_export[0]} / 🟩 export {import_export[1]}")
    itembtn_с2 = types.KeyboardButton(f"❓Что у нас есть?")



    markup_main.row(itembtn_a1, itembtn_a2, itembtn_a3, itembtn_a4, itembtn_a5)
    markup_main.row(itembtn_b1, itembtn_b2, itembtn_b3, itembtn_b4, itembtn_b5)
    markup_main.row(itembtn_с1, itembtn_с2, itembtn_c3)




bot = telebot.TeleBot("6328538971:AAGvF3WOaHtEufVQ2kRIwT-3cIFdnz67Dfo")

MODE = "NO"

update_objects()


@bot.message_handler(commands=['start'])
def start_message(message):
    global MODE
    bot.send_message(message.chat.id, 'Начинаем!', reply_markup=markup_main)
    MODE = "NO"


@bot.message_handler(content_types=['text'])
def deal_with_text(message):
    global MODE
    modes = ['WIND', 'SUN', 'HOUSE', 'HOSPITAL', 'FACTORY']
    if MODE in modes:
        try:
            float(message.text)
        except ValueError:
            bot.send_message(message.chat.id, 'Это не число', reply_markup=markup_main)
            MODE = "NO"
            return
        if float(message.text) > 0:
            data = open("input/objects.txt").read().split()
            idx = modes.index(MODE)
            data[idx] = int(data[idx]) + 1
            open("input/objects.txt", 'w').write(f"{data[0]} {data[1]} {data[2]} {data[3]} {data[4]}")
            with open("input/" + MODE, 'a') as f:
                f.write(f" {message.text}")
            update_objects()
            bot.send_message(message.chat.id, 'Успешно записано', reply_markup=markup_main)
        if float(message.text) < 0:
            # print("do del")
            f = open("input/" + MODE)
            data = list(map(float, f.read().split()))
            num = abs(float(message.text))
            if num not in data:
                bot.send_message(message.chat.id, 'Объект не найден', reply_markup=markup_main)
                MODE = 'NO'
                return
            data.remove(num)
            f = open("input/" + MODE, 'w')
            for d in data:
                f.write(str(d) + " ")
            f.close()

            data = open("input/objects.txt").read().split()
            idx = modes.index(MODE)
            data[idx] = int(data[idx]) - 1
            open("input/objects.txt", 'w').write(f"{data[0]} {data[1]} {data[2]} {data[3]} {data[4]}")
            update_objects()
            bot.send_message(message.chat.id, 'Объект успешно удалён', reply_markup=markup_main)
        MODE = 'NO'

    if '💨' in message.text:
        bot.send_message(message.chat.id, 'Введите стоимость ветрогенератора', reply_markup=markup_main)
        MODE = "WIND"
    if '☀️' in message.text:
        bot.send_message(message.chat.id, 'Введите стоимость солнечной панели', reply_markup=markup_main)
        MODE = "SUN"
    if '🏠' in message.text:
        bot.send_message(message.chat.id, 'Введите стоимость дома', reply_markup=markup_main)
        MODE = "HOUSE"
    if '🏥' in message.text:
        bot.send_message(message.chat.id, 'Введите стоимость больницы', reply_markup=markup_main)
        MODE = "HOSPITAL"
    if '🏭' in message.text:
        bot.send_message(message.chat.id, 'Введите стоимость завода', reply_markup=markup_main)
        MODE = "FACTORY"

    if message.text == '📊Прогноз':
        photo = open('output/forecast.png', 'rb')
        bot.send_photo(message.chat.id, photo)
    if message.text == '📊Баланс':
        photo = open('output/balans.png', 'rb')
        bot.send_photo(message.chat.id, photo)
    if message.text == '📊Детализированный':
        photo = open('output/categories.png', 'rb')
        bot.send_photo(message.chat.id, photo)
    if message.text == '📊Потребление/генерация':
        photo = open('output/cons_gen.png', 'rb')
        bot.send_photo(message.chat.id, photo)
    if message.text == '📊Деньги':
        photo = open('output/money.png', 'rb')
        bot.send_photo(message.chat.id, photo)
    if message.text == 'Докупка/продажа':
        my_text = ''
        with open('output/buysell.txt') as f1:
            my_text = f1.read()
            bot.send_message(message.chat.id, my_text)


    if message.text == "❓Что у нас есть?":
        modes = ['WIND', 'SUN', 'HOUSE', 'HOSPITAL', 'FACTORY']
        texts = ["💨Ветрогенераторы по: ", "☀️Солнечные панели по: ", "🏠Дома по: ", "🏥Больницы по: ", "🏭Заводы по: "]
        text_to_send = 'Список ваших объектов: '

        for i in range(5):
            f = open('input/' + modes[i])
            data = list(map(float, f.read().split()))
            if len(data) > 0:
                text_to_send += '\n' + texts[i]
            for d in data:
                text_to_send += str(d) + " "
        bot.send_message(message.chat.id, text_to_send)



@bot.message_handler(content_types=['document'])
def load_csv(message):
    raw = message.document.file_id
    path = "input/forecast.csv"
    file_info = bot.get_file(raw)
    downloaded_file = bot.download_file(file_info.file_path)
    with open(path, 'wb') as new_file:
        new_file.write(downloaded_file)
    with open("input/objects.txt", 'w') as f:
        f.write("0 0 0 0 0")
    modes = ['WIND', 'SUN', 'HOUSE', 'HOSPITAL', 'FACTORY']
    for mode in modes:
        f = open("input/" + mode, 'w')
        f.close()
    update_objects()
    bot.send_message(message.chat.id, 'Файл успешно загружен', reply_markup=markup_main)


bot.infinity_polling()
