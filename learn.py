from matplotlib import pyplot as plt
import pandas as pd

from sklearn.datasets import load_iris
from sklearn import tree
import pickle

names_gens = [
    ["Солнце", "sun.txt", "sun_gen.txt", "sun.clf"],
    ["Ветер", "wind.txt", "wind_gen.txt", "wind.clf"],
]

names_consumers = [
    ["Заводы", "factories.txt", "factories.clf"],
    ["Больницы", "hospitals.txt", "hospitals.clf"],
    ["Дома", "houses.txt", "houses.clf"]
]


def learn_gen(name_csv, name, name_gen, name_clf, name_forecast):
    info = []
    info_gen = []

    df = pd.read_csv(name_forecast, delimiter='\t')

    f = open(name)

    for line in f:
        info.append(float(line))

    f = open(name_gen)

    for line in f:
        info_gen.append([int(list(map(float, line.split()))[0] * 1000)])

    forecast = []

    for s in df[name_csv]:
        forecast.append([int(s * 1000)])

    # print(forecast)
    # print(info_gen)
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(forecast[:160], info_gen[:160])
    # print(forecast)
    # print(info_gen)
    s = pickle.dumps(clf)
    with open(name_clf, "wb") as f:
        f.write(s)


def learn_consumer(name_csv, name, name_clf, name_forecast):
    info = []
    df = pd.read_csv(name_forecast, delimiter='\t')

    f = open(name)
    for line in f:
        info.append([int(float(line.split()[0]) * 1000)])

    forecast = []

    for s in df[name_csv]:
        forecast.append([int(s * 1000)])

    clf = tree.DecisionTreeClassifier()
    # print(forecast)
    # print(info)
    clf = clf.fit(forecast[:160], info[:160])
    s = pickle.dumps(clf)
    with open(name_clf, "wb") as f:
        f.write(s)


directory = "experiment2/"
for name in names_gens:
    learn_gen(name[0], directory + name[1], directory + name[2], "models/" + name[3], directory + "forecast.csv")

for name in names_consumers:
    learn_consumer(name[0], directory + name[1], "models/" + name[2], directory + "forecast.csv")

